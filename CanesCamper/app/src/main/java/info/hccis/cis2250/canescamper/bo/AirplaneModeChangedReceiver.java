package info.hccis.cis2250.canescamper.bo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AirplaneModeChangedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        boolean isAirplaneModeEnable = intent.getBooleanExtra("state", false);
        if (isAirplaneModeEnable) {
            Toast.makeText(context, "Airplane Mode Enable", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(context, "Airplane Mode Disable", Toast.LENGTH_LONG).show();
        }
    }
}
